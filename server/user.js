const express=require('express');
const db=require('./db');
const utils=require('./utils');

const router=express.Router();

router.post('/customer/signin',(request,response)=>{
    console.log('customer sign in requested..');

    const { Cusername , Cpassword }=request.body;

    const connection=db.connect();
    const statement=`select * from Customer where Cusername='${Cusername}' and Cpassword='${Cpassword}' `;

    connection.query(statement,(error,result)=>{
        connection.end();

        let status='';
        let data=null;

        if(error == null){
            if(result.length == 0){
                status : error;
                data : 'invalid username or password';
            }
            else{
                status : 'success';
                data : result[0];
            }
        }else{
            status : 'error';
            data : error;
        }
        response.send({
            status : status,
            data : data
        });
    });
});


router.post('/customer/signup',(request,response)=>{
    console.log('customer requested for sign up ');

    const { name,address,email,password,phone }=request.body;
    const connection=db.connect();
    const statement=`insert into Customer 
                        (Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender) 
                    values('${Cname}','${Carea}','${Pin}','${Phone}'
                    ,'${Cemail}','${Cusername}','${Cpassword}','${Cgender}') `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

module.exports=router;

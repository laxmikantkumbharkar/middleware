const express=require('express');
const db=require('./db');
const utils=require('./utils');
const multer = require('multer');
const upload = multer({dest: 'images/'});
const router=express.Router();


// Product related services
//========================================================================================
router.get('/product',(request,response)=>{
    console.log('product get method requested');

    const statement=`select Pid,Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/product/:Pid',(request,response)=>{
    console.log('product get method requested');
    const Pid=request.params.Pid;
    const statement=`select Pname,Prate,Pcategory,PQuantity,Pthumbnail from Products where Pid='${Pid}' `;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result[0]));
    });
});

router.post('/product',upload.single('Pthumbnail'),((request,response)=>{
    console.log('product post method requested');
    const {Pname,Prate,Pcategory,PQuantity } = request.body;

    const statement=`insert into Products (Pname,Prate,Pcategory,PQuantity,Pthumbnail) 
                    values('${Pname}','${Prate}','${Pcategory}','${PQuantity}','${request.file.filename}')`;
    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
}));

router.put('/product/:Pid',(request,response)=>{
    console.log('product put method requested');
    const Pid=request.params.Pid;
    const { Pname,Prate,Pcategory,PQuantity }=request.body;
    const statement=`update Products set 
                    Pname='${Pname}',Prate='${Prate}',
                    Pcategory='${Pcategory}',PQuantity='${PQuantity}'
                    where Pid='${Pid}' `;
    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/product/:id',(request,response)=>{
    console.log('product delete method requested..');

    const Pid=request.params.id;
    
    const statement=`delete from Products where Pid='${Pid}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

// Milk Rate Table Services
//========================================================================================
router.get('/milkrate',(request,response)=>{
    console.log('milk rate table get method requested .... ');
    const connection=db.connect();
    const statement=`select 
                    Fat , Buffalo,Cow ,Updated_on from Milk_rate`;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    }); 
});


router.post('/milkrate',(request,response)=>{
    console.log('milk rate tables post method requested..');
  
    const { Fat ,Buffalo ,Cow ,Updated_on }=request.body;
    
    const connection=db.connect();
    
    const statement=`insert into Milk_rate 
                    (Fat , Buffalo, Cow ,Updated_on ) 
                    values
                    ('${Fat}','${Buffalo}','${Cow}',NOW()) `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


router.put('/milkrate/:date',(request,response)=>{
    console.log('milk rate tables put method requested..');
    const Updated_on=request.params.date;
    const { Fat , Buffalo, Cow }=request.body;
    
    const connection=db.connect();
    
    const statement=`update Milk_rate set
                        Fat='${Fat}',Buffalo='${Buffalo}',Cow='${Cow}' 
                    where 
                        Updated_on='${Updated_on}' `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/milkrate/:Fat',(request,response)=>{
    console.log('milk rate delete method requested..');
    const Fat=request.params.Fat;
    
    const connection=db.connect();
    
    const statement=`delete from Milk_rate where Fat='${Fat}' `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

//Customers related services
//=============================================================================================

router.get('/customer',(request,response)=>{
    console.log('customer get method requested');

    const statement=`select Cid,Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail from Customer`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.get('/customer/:Cid',(request,response)=>{
    console.log('customer get method requested');
    const Cid=request.params.Cid;
    const statement=`select Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail from Customer where Cid='${Cid}'`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/customer',upload.single('Cthumbnail'),(request,response)=>{
    console.log('customer post method requested');
    const {Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail } = request.body;

    const statement=`insert into Customer 
                        (Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail) 
                    values
                    ('${Cname}','${Carea}','${Pin}','${Phone}',
                    '${Cemail}','${Cusername}','${Cpassword}','${Cgender}','${Cthumbnail}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/customer/:id',(request,response)=>{
    console.log('customer put method requested');
    const Cid=request.params.id;
    const { Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender,Cthumbnail }=request.body;
    const statement=`update Customer set 
                        Cname='${Cname}', Carea='${Carea}',Pin='${Pin}',
                        Phone='${Phone}', Cemail='${Cemail}', 
                        Cusername='${Cusername}',Cpassword='${Cpassword}',
                        Cgender='${Cgender}',Cthumbnail='${Cthumbnail}' 
                    where 
                        Cid='${Cid}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/customer/:id',(request,response)=>{
    console.log('customer delete method requested..');

    const Cid=request.params.id;
    
    const statement=`delete from Customer where Cid='${Cid}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/customer/signin',(request,response)=>{
    console.log('customer sign in requested..');

    const { Cusername , Cpassword }=request.body;

    const connection=db.connect();
    const statement=`select * from Customer where Cusername='${Cusername}' and Cpassword='${Cpassword}' `;

    connection.query(statement,(error,result)=>{
        connection.end();

        let status='';
        let data=null;

        if(error == null){
            if(result.length == 0){
                status : error;
                data : 'invalid username or password';
            }
            else{
                status : 'success';
                data : result[0];
            }
        }else{
            status : 'error';
            data : error;
        }
        response.send({
            status : status,
            data : data
        });
    });
});


router.post('/customer/signup',(request,response)=>{
    console.log('customer requested for sign up ');

    const { name,address,email,password,phone }=request.body;
    const connection=db.connect();
    const statement=`insert into Customer 
                        (Cname,Carea,Pin,Phone,Cemail,Cusername,Cpassword,Cgender) 
                    values('${Cname}','${Carea}','${Pin}','${Phone}'
                    ,'${Cemail}','${Cusername}','${Cpassword}','${Cgender}') `;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


//Dealers related services
//=============================================================================================

router.get('/dealer',(request,response)=>{
    console.log('dealer get method requested');

    const statement=`select Did,Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail from Dealers`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/dealer',(request,response)=>{
    console.log('dealer post method requested');
    const { Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail } = request.body;

    const statement=`insert into Dealers 
                        ( Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail) 
                    values
                    ('${Dname}','${Daddress}','${Dphone}','${Agency_name}','${Dusername}',
                    '${Dpassword}','${Age}','${Gender}','${Demail}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/dealer/:id',(request,response)=>{
    console.log('dealer put method requested');
    const Did=request.params.id;
    const { Dname,Daddress,Dphone,Agency_name,Dusername,Dpassword,Age,Gender,Demail }=request.body;
    const statement=`update Dealers set 
                        Dname='${Dname}', Daddress='${Daddress}',
                        Dphone='${Dphone}',Agency_name='${Agency_name}',
                        Dusername='${Dusername}', Dpassword='${Dpassword}', 
                        Age='${Age}',Gender='${Gender}',Demail='${Demail}' 
                    where 
                        Did='${Did}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/dealer/:id',(request,response)=>{
    console.log('dealer delete method requested..');

    const Did=request.params.id;
    
    const statement=`delete from Dealers where Did='${Did}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

//Staff related services
//=============================================================================================

router.get('/staff',(request,response)=>{
    console.log('staff get method requested');

    const statement=`select Sid,Sname,Saddress,Semail,DeptId,Srole,Ssalary,
                        Susername,Spassword,Sgender,Sage,Sphone from Staff`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/staff',(request,response)=>{
    console.log('staff post method requested');
    const { Sname,Saddress,Semail,DeptId,Srole,Ssalary,
            Susername,Spassword,Sgender,Sage,Sphone } = request.body;

    const statement=`insert into Staff 
                        ( Sname,Saddress,Semail,DeptId,Srole,Ssalary,
                            Susername,Spassword,Sgender,Sage,Sphone) 
                    values
                    ('${Sname}','${Saddress}','${Semail}','${DeptId}','${Srole}',
                    '${Ssalary}','${Susername}','${Spassword}','${Sgender}','${Sage}','${Sphone}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/staff/:id',(request,response)=>{
    console.log('staff put method requested');
    const Sid=request.params.id;
    const { Sname,Saddress,Semail,DeptId,Srole,Ssalary,
            Susername,Spassword,Sgender,Sage,Sphone }=request.body;
    const statement=`update Staff set 
                        Sname='${Sname}', Saddress='${Saddress}',
                        Semail='${Semail}',DeptId='${DeptId}',
                        Srole='${Srole}', Ssalary='${Ssalary}', 
                        Susername='${Susername}',Spassword='${Spassword}',Sgender='${Sgender}',
                        Sage='${Sage}',Sphone='${Sphone}' 
                    where 
                        Sid='${Sid}' `;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/staff/:id',(request,response)=>{
    console.log('staff delete method requested..');

    const Sid=request.params.id;
    
    const statement=`delete from Staff where Sid='${Sid}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


//Feedback page related services
//===========================================================================================

router.get('/feedback',(request,response)=>{
    console.log('feedback get method requested');

    const statement=`select cust_id,Name,Address,Taluka,District,Mobile,Email,fb,date,Response from Feedback`;

    const connection = db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.post('/feedback',(request,response)=>{
    console.log('feedback post method requested');
    const {Name,Address,Taluka,District,Mobile,Email,fb,date,Response } = request.body;

    const statement=`insert into Feedback 
                        (Name,Address,Taluka,District,Mobile,Email,fb,date,Response) 
                    values
                    ('${Name}','${Address}','${Taluka}','${District}',
                    '${Mobile}','${Email}','${fb}','${date}','${Response}' )`;

    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResponse(error,result));
    });
});

router.put('/feedback/:id',(request,response)=>{
    console.log('feedback put method requested');
    const cust_id=request.params.id;
    const { Response }=request.body;
    const statement=`update Feedback set 
                        Response='${Response}'
                    where cust_id='${cust_id}' `;
    const connection=db.connect();

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});

router.delete('/feedback/:id',(request,response)=>{
    console.log('feedback delete method requested..');

    const cust_id=request.params.id;
    
    const statement=`delete from Feedback where cust_id='${cust_id}' `;
    const connection=db.connect();
    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResponse(error,result));
    });
});


module.exports=router;

